#include <stdint.h>
#include <stdbool.h>
#include "inc/hw_memmap.h"
#include "inc/hw_types.h"
#include "driverlib/debug.h"
#include "driverlib/fpu.h"
#include "driverlib/gpio.h"
#include "driverlib/pin_map.h"
#include "driverlib/rom.h"
#include "driverlib/sysctl.h"
#include "driverlib/uart.h"
#include "utils/uartstdio.h"

#include "DIO_F.h"
//*****************************************************************************
//
// Configure the GPIO API to initialize the GPIO peripheral.
//
//*****************************************************************************
void 
ConfigurePorts(void){
    // Enable the GPIOB peripheral
    ROM_SysCtlPeripheralEnable(SYSCTL_PERIPH_GPIOB);
    // Wait for the GPIOA module to be ready.
    while(!ROM_SysCtlPeripheralReady(SYSCTL_PERIPH_GPIOB))
        {
        }
    // Enable the GPIOE peripheral
    ROM_SysCtlPeripheralEnable(SYSCTL_PERIPH_GPIOE);
    // Wait for the GPIOA module to be ready.
    while(!ROM_SysCtlPeripheralReady(SYSCTL_PERIPH_GPIOE))
        {
        }
    // Enable the GPIO port F that is used for the on-board LED.
    ROM_SysCtlPeripheralEnable(SYSCTL_PERIPH_GPIOF);

    // Check if the peripheral access is enabled.
    while(!ROM_SysCtlPeripheralReady(SYSCTL_PERIPH_GPIOF))
    {
    }
    // Set pins 1, 2, 3, 4 and 5 as input of port B
    ROM_GPIOPinTypeGPIOInput(GPIO_PORTB_BASE,
    GPIO_PIN_0 | GPIO_PIN_1 | GPIO_PIN_2 | GPIO_PIN_3 | GPIO_PIN_4
    | GPIO_PIN_5);
    
        // Set pins 4 and 5 as input of port E
    ROM_GPIOPinTypeGPIOInput(GPIO_PORTE_BASE,
    GPIO_PIN_4 | GPIO_PIN_5);
    
     // Enable the GPIO pin for the LED (PF1).  Set the direction as output, and
    // enable the GPIO pin for digital function.
    //
    ROM_GPIOPinTypeGPIOOutput(GPIO_PORTF_BASE, GPIO_PIN_2);
}

//*****************************************************************************


