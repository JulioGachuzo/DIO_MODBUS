//*****************************************************************************
//
// DIO_MODBUS_FERROVIARIO_V1.c - Secuencial code without tasks.
// 14 de agosto de 2017
// Copyright (c) 2012-2017 Julio Gachuzo.  All rights reserved.
// Software License Agreement
// This is part of revision 2.1.4.178 of the EK-TM4C123GXL Firmware Package.
//
//*****************************************************************************
#include <stdint.h>
#include <stdbool.h>
#include "inc/hw_ints.h"
#include "inc/hw_memmap.h"
#include "inc/hw_types.h"
#include "driverlib/debug.h"
#include "driverlib/fpu.h"
#include "driverlib/gpio.h"
#include "driverlib/interrupt.h"
#include "driverlib/pin_map.h"
#include "driverlib/rom.h"
#include "driverlib/sysctl.h"
#include "driverlib/uart.h"
#include "utils/uartstdio.h"
#include "DIO_F.h"
//*****************************************************************************
//
//! Descripcion breve del codigo
//!     1.- Inicializar configuracion del TIVA 123 
//!             -Puertos
//
//*****************************************************************************
//*****************************************************************************
//*****************************************************************************
//
// The error routine that is called if the driver library encounters an error.
//
//*****************************************************************************
#ifdef DEBUG
void
__error__(char *pcFilename, uint32_t ui32Line)
{
}
#endif


int
main(void)
{
    // Enable lazy stacking for interrupt handlers.  This allows floating-point
    // instructions to be used within interrupt handlers, but at the expense of
    // extra stack usage.
    //
    ROM_FPUEnable();
    ROM_FPULazyStackingEnable();
    //
    //Configure ports B & E
    ConfigurePorts();
    //
    //Clock config
    ROM_SysCtlClockSet(SYSCTL_SYSDIV_1 | SYSCTL_USE_OSC | SYSCTL_OSC_MAIN |
                       SYSCTL_XTAL_16MHZ);
    //Local variables to stock the value of the port B & E
    volatile uint8_t port_B;
    volatile uint8_t port_E;
    
    // Loop forever.
    while(1)
    {
        //read port B
        port_B = GPIOPinRead(GPIO_PORTB_BASE,(GPIO_PIN_0 | GPIO_PIN_1 | 
                  GPIO_PIN_2 | GPIO_PIN_3 | GPIO_PIN_4 | GPIO_PIN_5));
        //read port E
        port_E = GPIOPinRead(GPIO_PORTE_BASE,(GPIO_PIN_4 | GPIO_PIN_5));
        
    }
}


