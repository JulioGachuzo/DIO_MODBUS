#ifndef __DIO_F__
#define __DIO_F__
void 
ConfigurePorts(void);
void
UARTIntHandler(void);
void
UARTSend(const uint8_t *pui8Buffer, uint32_t ui32Count);
int32_t 
DigitalRead(void);
int32_t 
Logica(void);
int32_t 
IDread(void);
int32_t 
LogicaSensor1(void);
extern volatile uint32_t flag1;  /* Declaration of the variable */

#endif // __DIO_F__