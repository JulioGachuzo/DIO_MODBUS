//*****************************************************************************
//
// DIO_MODBUS_FERROVIARIO_UART_V2.c - Secuencial code without tasks and UART.
// 16 de agosto de 2017
// Copyright (c) 2012-2017 Julio Gachuzo.  All rights reserved.
// Software License Agreement
// This is part of revision 2.1.4.178 of the EK-TM4C123GXL Firmware Package.
//
//*****************************************************************************
#include <stdint.h>
#include <stdbool.h>
#include "inc/hw_ints.h"
#include "inc/hw_memmap.h"
#include "inc/hw_types.h"
#include "driverlib/debug.h"
#include "driverlib/fpu.h"
#include "driverlib/gpio.h"
#include "driverlib/interrupt.h"
#include "driverlib/pin_map.h"
#include "driverlib/rom.h"
#include "driverlib/sysctl.h"
#include "driverlib/uart.h"
#include "utils/uartstdio.h"
#include "DIO_F.h"
#include "tm4c123gh6pm.h"

//*****************************************************************************
//
//! Descripcion breve del codigo
//!     1.- Inicializar configuracion del TIVA 123 
//!             *-Puertos
//!             *-UART
//!             -Interrupcciones
//!             -Reloj
//!             -Variables
//!             -Funciones
//!     2.- Leer y enviar pulso al WDT
//!             -Leer 8 entradas digitales
//!             -Enviar un pulso al WDT externo cada 0.5 segundos
//! UART0, connected to the Virtual Serial Port and running at
//! 115,200, 8-N-1, is used to display messages from this application.
//
//*****************************************************************************
//*****************************************************************************
//*****************************************************************************
//
// The error routine that is called if the driver library encounters an error.
//
//*****************************************************************************
#ifdef DEBUG
void
__error__(char *pcFilename, uint32_t ui32Line)
{
}
#endif
int
main(void)
{
    // Enable lazy stacking for interrupt handlers.  This allows floating-point
    // instructions to be used within interrupt handlers, but at the expense of
    // extra stack usage.
    //
    ROM_FPUEnable();
    ROM_FPULazyStackingEnable();
    //
    //Configure ports B & E
    ConfigurePorts();
    //
    //Clock config
    ROM_SysCtlClockSet(SYSCTL_SYSDIV_1 | SYSCTL_USE_OSC | SYSCTL_OSC_MAIN |
                       SYSCTL_XTAL_16MHZ);
    //Local variables to stock the value of the port B & E
    volatile uint32_t variable;
    volatile uint32_t ui32Loop;
    //
    //Enable processor interrupts.
    //
    ROM_IntMasterEnable();
    //
    // Set GPIO A0 and A1 as UART pins.
    //
    ROM_GPIOPinConfigure(GPIO_PA0_U0RX);
    ROM_GPIOPinConfigure(GPIO_PA1_U0TX);
    ROM_GPIOPinTypeUART(GPIO_PORTA_BASE, GPIO_PIN_0 | GPIO_PIN_1);
        //
    // Configure the UART for 115,200, 8-N-1 operation.
    //
    ROM_UARTConfigSetExpClk(UART0_BASE, ROM_SysCtlClockGet(), 115200,
                            (UART_CONFIG_WLEN_8 | UART_CONFIG_STOP_ONE |
                             UART_CONFIG_PAR_NONE));
    //
    // Enable the UART interrupt.
    //
    ROM_IntEnable(INT_UART0);
    ROM_UARTIntEnable(UART0_BASE, UART_INT_RX | UART_INT_RT);
    //
    while(1)
    {
      // Leer el valor del ID
      IDread();
      // Delay for a bit.
     //   while (ui32Loop++ < 500000){
       //   ui32Loop++;
        //}
      variable = DigitalRead();
     //for (int i= 0 ... 
      // if ( variable << i
      //flag 

         // Turn on the LED.
        ROM_GPIOPinWrite(GPIO_PORTF_BASE, GPIO_PIN_1, GPIO_PIN_1);
        // Delay for a bit.
        while (ui32Loop++ < 500000){
          ui32Loop++;
        }
        // Turn off the LED.
        ROM_GPIOPinWrite(GPIO_PORTF_BASE, GPIO_PIN_1, 0x0);
        // Delay for a bit.
        for(ui32Loop = 0; ui32Loop < 200000; ui32Loop++)
        {
        }
    }
}


