#include <stdint.h>
#include <stdbool.h>
#include "inc/hw_memmap.h"
#include "inc/hw_types.h"
#include "driverlib/debug.h"
#include "driverlib/fpu.h"
#include "driverlib/gpio.h"
#include "driverlib/pin_map.h"
#include "driverlib/rom.h"
#include "driverlib/sysctl.h"
#include "driverlib/uart.h"
#include "utils/uartstdio.h"
#include "DIO_F.h"

//Entradas digitales
#define PE5 ((uint32_t *)0x40024080)// 0
#define PE4 ((uint32_t *)0x40024040)
#define PB4 ((uint32_t *)0x40005040)
#define PB5 ((uint32_t *)0x40005080)
#define PB3 ((uint32_t *)0x40005020)
#define PB2 ((uint32_t *)0x40005010)
#define PB1 ((uint32_t *)0x40005008)
#define PB0 ((uint32_t *)0x40005004)

//ID
#define PD0 ((uint32_t *)0x40007004)
#define PD1 ((uint32_t *)0x40007008)
#define PD2 ((uint32_t *)0x40007010)
#define PD3 ((uint32_t *)0x40007020)
#define PE3 ((uint32_t *)0x40024020)
#define PE2 ((uint32_t *)0x40024010)
#define PE1 ((uint32_t *)0x40024008)
#define PE0 ((uint32_t *)0x40024004)

//
volatile uint32_t flag1 = 0;

//*****************************************************************************
//*****************************************************************************
//
//*****************************************************************************
//
// Configure the GPIO API to initialize the GPIO peripheral.
//
//*****************************************************************************
void ConfigurePorts(void){
    // Enable the GPIO B peripheral
    ROM_SysCtlPeripheralEnable(SYSCTL_PERIPH_GPIOB);
    // Wait for the GPIO B module to be ready.
    while(!ROM_SysCtlPeripheralReady(SYSCTL_PERIPH_GPIOB)) {
        }
    // Enable the GPIO E peripheral
    ROM_SysCtlPeripheralEnable(SYSCTL_PERIPH_GPIOE);
    // Wait for the GPIO E module to be ready.
    while(!ROM_SysCtlPeripheralReady(SYSCTL_PERIPH_GPIOE)){
        }
    // Enable the GPIO port F that is used for the on-board LED.
    ROM_SysCtlPeripheralEnable(SYSCTL_PERIPH_GPIOF);

    // Wait for the GPIO F module to be ready.
    while(!ROM_SysCtlPeripheralReady(SYSCTL_PERIPH_GPIOF)){
    }
    // Enable the UART 0.
    ROM_SysCtlPeripheralEnable(SYSCTL_PERIPH_UART0);
    // Check if the peripheral access is enabled.
    while(!ROM_SysCtlPeripheralReady(SYSCTL_PERIPH_UART0)){
    }
    // Enable the GPIO A peripheral
    ROM_SysCtlPeripheralEnable(SYSCTL_PERIPH_GPIOA);
    // Check if the peripheral access is enabled.
    while(!ROM_SysCtlPeripheralReady(SYSCTL_PERIPH_GPIOA)){
    }
        // Enable the GPIO D peripheral
    ROM_SysCtlPeripheralEnable(SYSCTL_PERIPH_GPIOD);
    // Check if the peripheral access is enabled.
    while(!ROM_SysCtlPeripheralReady(SYSCTL_PERIPH_GPIOD)){
    }
    //
    // Set pins 1, 2, 3, 4 and 5 as input of port B
    ROM_GPIOPinTypeGPIOInput(GPIO_PORTB_BASE,
    GPIO_PIN_0 | GPIO_PIN_1 | GPIO_PIN_2 | GPIO_PIN_3 | GPIO_PIN_4
    | GPIO_PIN_5);
    
    // Set pins 0, 1, 2, 3, 4 and 5 as input of port E
    ROM_GPIOPinTypeGPIOInput(GPIO_PORTE_BASE,
    GPIO_PIN_0 | GPIO_PIN_1 | GPIO_PIN_2 | GPIO_PIN_3 | GPIO_PIN_4 
    | GPIO_PIN_5);
    
    // Set pins 0, 1, 2, 3 as input of port D
    ROM_GPIOPinTypeGPIOInput(GPIO_PORTD_BASE,
    GPIO_PIN_0 | GPIO_PIN_1 | GPIO_PIN_2 | GPIO_PIN_3);
    
     // Enable the GPIO pin for the LED (PF1).  Set the direction as output, and
    // enable the GPIO pin for digital function.
    //
    ROM_GPIOPinTypeGPIOOutput(GPIO_PORTF_BASE, (GPIO_PIN_1 | GPIO_PIN_2 | 
                                                GPIO_PIN_3));
}

//*****************************************************************************
//*****************************************************************************
//
// The UART interrupt handler.
//
//*****************************************************************************
void
UARTIntHandler(void)
{
    uint32_t ui32Status;
    //
    // "Get the interrrupt status"
    //The Masked Interrupt Register is an AND of Raw Interrupt Status and Interrupt 
    //Mssk. The result of the Mask Interrupt Status is then OR-ed to generate 
    //an interrupt to the Cortex M4.
    ui32Status = ROM_UARTIntStatus(UART0_BASE, true);
    //
    // Clear the asserted interrupts.
    //This function must be called in the interrupt handler to keep the 
    //interrupt from being triggered again immediately upon exit.
    ROM_UARTIntClear(UART0_BASE, ui32Status);
    //
    // Loop while there are characters in the receive FIFO.
    ////Determines if there are any characters in the receive FIFO.
    while(ROM_UARTCharsAvail(UART0_BASE))
    {
        //
        // Read the next character from the UART and write it back to the UART.
        //Sends a character to the specified port.
        //Returns true if the character was successfully placed in the transmit 
        //FIFO or false if there was no space available in the transmit FIFO.
        ROM_UARTCharPutNonBlocking(UART0_BASE,
                                   ROM_UARTCharGetNonBlocking(UART0_BASE));
                                    //Receives a character from the specified port.
        //
        // Blink the LED to show a character transfer is occuring.
        //
        GPIOPinWrite(GPIO_PORTF_BASE, GPIO_PIN_2, GPIO_PIN_2);

        //
        // Delay for 1 millisecond.  Each SysCtlDelay is about 3 clocks.
        //
        SysCtlDelay(SysCtlClockGet() / (1000U * 3U));

        //
        // Turn off the LED
        //
        GPIOPinWrite(GPIO_PORTF_BASE, GPIO_PIN_2, 0U);
         // Prompt for text to be entered.
    // Prompt for text to be entered.

    }
}
//*****************************************************************************
//
// Send a string to the UART.
//
//*****************************************************************************
void
UARTSend(const uint8_t *pui8Buffer, uint32_t ui32Count)
{
    // esta funcion solo sirve para enviar el "Enter text: " AL MOMENTO DE 
    // INICIAR LA CONEXION CON LA TERMINAL
    // Loop while there are more characters to send.
    //
    while(ui32Count--)
    {
        //
        // Write the next character to the UART.
        //
        ROM_UARTCharPutNonBlocking(UART0_BASE, *pui8Buffer++);
    }
}

//*****************************************************************************
//*****************************************************************************
//
// Lectura de entradas digitales.
//
//*****************************************************************************
int32_t 
DigitalRead(void){
   uint32_t digitalinputs;
   
   uint32_t j0;
   uint32_t j1;
   uint32_t j2;
   uint32_t j3;
   uint32_t j4;
   uint32_t j5;
   uint32_t j6;
   uint32_t j7;
   
  //read port B

   
        uint32_t *EntradasDigitales[8]= {PE5, PE4, PB4, PB5, PB3, PB2, PB1, PB0};
        j0 = (*EntradasDigitales[0] >> 5U);  //"E0/6U"
        j1 = (*EntradasDigitales[1] >> 3U);  //"E0/6U"
        j2 = (*EntradasDigitales[2] >> 2U);
        j3 = (*EntradasDigitales[3] >> 2U); //izquierda << "B4*2U"
        j4 = (*EntradasDigitales[4] << 1U);
        j5 = (*EntradasDigitales[5] << 3U);
        j6 = (*EntradasDigitales[6] << 5U);
        j7 = (*EntradasDigitales[7] << 7U);
        digitalinputs = (j0 | j1 | j2 | j3 | j4 | j5 | j6 | j7);
        return digitalinputs;
}
//*****************************************************************************
//*****************************************************************************
//
// Lectura de ID
//
//*****************************************************************************
int32_t 
IDread(void){
   uint32_t IDidentifier;
   uint32_t ID0;
   uint32_t ID1;
   uint32_t ID2;
   uint32_t ID3;
   uint32_t ID4;
   uint32_t ID5;
   uint32_t ID6;
   uint32_t ID7;
               
        uint32_t *ID[8]= {PD0, PD1, PD2, PD3, PE3, PE2, PE1, PE0};
        
        ID0 = *ID[0];  
        ID1 = *ID[1];  
        ID2 = *ID[2];
        ID3 = *ID[3]; //izquierda << "B4*2U"
        ID4 = (*ID[4] << 1U);
        ID5 = (*ID[5] << 3U);
        ID6 = (*ID[6] << 5U);
        ID7 = (*ID[7] << 7U);
        IDidentifier = (ID0 | ID1 | ID2 | ID3 | ID4 | ID5 | ID6 | ID7);
        return IDidentifier;
}
//*****************************************************************************
//*****************************************************************************
//
// Identificación de la logica del sensor
//
//*****************************************************************************
int32_t 
LogicaSensor1(void){
  
  //Dure 60 milisegundos {
  volatile uint32_t a = 0;
  a = ROM_GPIOPinRead(GPIO_PORTB_BASE,(GPIO_PIN_0));
//}
  if (a == 0){
    flag1 == 1;
  }
  else{
    flag1 == 0; 
  }
  
}
//*****************************************************************************